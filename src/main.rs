use std::time::Duration;

use atom_syndication::{EntryBuilder, FeedBuilder, LinkBuilder, PersonBuilder};
use chrono::{offset::Utc, DateTime};
use serde::{Deserialize, Serialize};
use surf::{Client, Config, Url};
use tide::{log, Request, Response, StatusCode};

const ATOM_MIME_TYPE: &str = "application/atom+xml";
const ATOM_ALTERNATE_REL: &str = "alternate";
const TWITCH_ENDPOINT: &str = "https://gql.twitch.tv/gql";
const TWITCH_TIMEOUT: Duration = Duration::from_secs(30);
const TWITCH_CHANNEL_URL_PREFIX: &str = "https://twitch.tv/";
const CLIENT_ID: &str = "Client-ID";
const TWITCH_CLIENT_ID: &str = "kimne78kx3ncx6brgo4mv6wki5h1ko";

#[derive(Deserialize)]
struct QueryResponse {
    data: Data,
}

#[derive(Deserialize)]
struct Data {
    user: Option<User>,
}

#[derive(Deserialize)]
struct User {
    stream: Option<Stream>,
    #[serde(rename = "profileImageURL")]
    profile_image_url: Option<String>,
}

#[derive(Deserialize)]
struct Stream {
    id: String,
    #[serde(rename = "createdAt")]
    created_at: DateTime<Utc>,
    title: String,
}

#[derive(Serialize)]
struct QueryRequest {
    query: String,
}

impl QueryRequest {
    fn with_channel(channel: &str) -> QueryRequest {
        QueryRequest {
            query: format!(
                "query {{ user(login: \"{}\") {{ stream {{ id, createdAt, title }} profileImageURL(width: 300) }}}}",
                channel
            ),
        }
    }
}

#[async_std::main]
async fn main() -> tide::Result<()> {
    let port = std::env::var("PORT")?;

    let twitch_client: Client = Config::new()
        .set_base_url(Url::parse(TWITCH_ENDPOINT)?)
        .set_timeout(Some(TWITCH_TIMEOUT))
        .try_into()?;
    let twitch_client = twitch_client.with(surf::middleware::Logger::new());

    let mut app = tide::with_state(twitch_client);
    log::start();

    app.at("/channel/:channel")
        .get(|req: Request<Client>| async move {
            let channel = req.param("channel")?;
            let QueryResponse {
                data: Data { user },
            } = req
                .state()
                .post("")
                .body_json(&QueryRequest::with_channel(channel))?
                .header(CLIENT_ID, TWITCH_CLIENT_ID)
                .recv_json()
                .await?;

            match user {
                Some(user) => {
                    let url = format!("{}{}", TWITCH_CHANNEL_URL_PREFIX, channel);
                    let link = LinkBuilder::default()
                            .rel(ATOM_ALTERNATE_REL.to_string())
                            .href(url.clone())
                            .build();
                    let author = PersonBuilder::default()
                            .name(channel.to_string())
                            .uri(Some(url))
                            .build();
                    let entries = if let Some(Stream {
                        id,
                        created_at,
                        title,
                    }) = user.stream
                    {
                        vec![EntryBuilder::default()
                            .id(&id)
                            .updated(created_at)
                            .title(title)
                            .link(link.clone())
                            .author(author.clone())
                            .build()]
                    } else {
                        vec![]
                    };
                    let response = FeedBuilder::default()
                        .icon(user.profile_image_url)
                        .updated(Utc::now())
                        .title(channel)
                        .author(author)
                        .link(link)
                        .id(channel)
                        .entries(entries)
                        .build();
                    Ok(Response::builder(StatusCode::Ok)
                        .content_type(ATOM_MIME_TYPE)
                        .body(response.to_string())
                        .build())
                }
                None => Ok(Response::new(StatusCode::NotFound)),
            }
        });
    app.listen(format!("0.0.0.0:{}", port)).await?;
    Ok(())
}
