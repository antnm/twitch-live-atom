A simple HTTP server for creating Atom feeds for Twitch livestreams without needing an account.

To run the `PORT` environment variable must be provided. There is also a systemd service file provided. Once the server is running, feeds can be retrieved like so: `localhost:8080/channel/name-of-the-followed-channel`.
